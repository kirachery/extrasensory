<?php

namespace App;

require_once "psychic.php";
class App
{
    public $psychics_count = 8;
    public $try = 0;


    function __construct() {
        session_start();

        if ($_SESSION['try'] == 0) {
            for ($i = 0; $i < $this->psychics_count; $i++) {
                $_SESSION["Psychics"][$i]["level"] = 0;
            }
        }

        if (isset($_SESSION["visit_count"])) {
            $this->try = $_SESSION["try"];
        }
        else {
            $_SESSION["visit_count"] = 1;
            $_SESSION["try"] = 0;

        };
        //$this->createPsychics();
    }

    public function play() {
        $psy = new Psychic();
        for ($i = 0;$i < $this->psychics_count; $i++) {
            $_SESSION["answers"][$this->try][$i] = $psy->guess();
        }

    }

    public function createPsychics() {
        for ($i = 1;$i <= $this->psychics_count; $i++) {
            $_SESSION["Psychics"][$i] = new Psychic();
        }
    }



    public function compare($nubmer) {
        foreach ($_SESSION["answers"][$_SESSION["try"]] as $guesser_id => $answer) {
            if ($answer == $nubmer) {
                $_SESSION["Psychics"][$guesser_id]["level"]++;
            }
            else {
                $_SESSION["Psychics"][$guesser_id]["level"]--;
            }
        }
        $_SESSION["try"]++;
        $_SESSION["right_answers"][$_SESSION["try"]-1] = $nubmer;
    }
}