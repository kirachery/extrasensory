<?php
session_unset();
require_once "App.php";
$app = new \App\App();
$done = true;

    if(isset($_POST['number'])) {
        $app->compare($_POST['number']);
    }
    else if (!isset($_POST["start"])){
        $app->play();
    }

$try = $_SESSION["try"];
if ($try == 0) {
    $done = true;
}
if (isset($try) && isset($_SESSION["answers"][$try])) {
    $done = false;

}
echo "<pre>";
print_r($_SESSION);
echo "</pre>";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container text-center">
<form action="index.php" method="post">
    <?php
        echo ($done === true) ? "<p>Mind some two-digit number please</p>" : "<p>And the number is</p><input type='number' name='number'>";
        echo ($try == 0) ? "<input type='hidden' name='start' value='yes'>" : "";
    ?>
    <input type="submit" value=" <?php echo ($done === true) ? "Done!" : "Is that"; ?>">
</div>

</form>
<table>
    <tbody>
    <?php
    foreach ($_SESSION["Psychics"] as $key => $psy) {
        echo "<tr><td>Psy #" . $key+1 . "</td><td>" . $psy["level"] . "</td><tr>";
    }
    ?>
    </tbody>
</table>
</body>
</html>